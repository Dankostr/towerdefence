package com.example.towerdefence

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import com.example.towerdefence.data.ImageResources
import com.example.towerdefence.game.GameActivity

class GameModeActivity : AppCompatActivity() {

    private val RANDOM_MODE = 1
    private val CAMPAIGN_MODE = 2

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.game_mode)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RANDOM_MODE) {
            return
        }
        if (requestCode == CAMPAIGN_MODE) {
            if (resultCode == Activity.RESULT_OK) {
                val lv = data!!.getIntExtra("Won", 0)
                if (lv != 0) {
                    val preferences = PreferenceManager.getDefaultSharedPreferences(applicationContext)
                    var levels = 1
                    if (preferences.contains("Level")) {
                        levels = preferences.getInt("Level", 1)
                        if (levels == lv) {
                            preferences.edit().putInt("Level", lv + 1).apply()
                        }
                    }
                }
            }
            else {
                return
            }
        }
    }

    fun randomGameModeClicked(view: View) {
        val intent = Intent(this, GameActivity::class.java)
        startActivityForResult(intent, RANDOM_MODE)
    }

    fun campaignClicked(view: View) {
        val intent = Intent(this, CampaignActivity::class.java)
        startActivityForResult(intent, CAMPAIGN_MODE)
    }
}