package com.example.towerdefence.data

import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.example.towerdefence.game.gameobjects.attackers.*
import com.example.towerdefence.game.gameobjects.towers.*
import com.example.towerdefence.game.gameobjects.towers.laser_towers.*
import com.example.towerdefence.game.gameobjects.towers.projectile_towers.*
import com.example.towerdefence.game.heroes.*
import com.example.towerdefence.information.attackers.AttackerInformationStorage
import com.example.towerdefence.information.heroes.HeroInformationStorage
import com.example.towerdefence.information.towers.TowerInformationStorage


class DatabaseHelper(mContext: Context) : SQLiteOpenHelper(mContext, DB_NAME, null, DB_VERSION) {


    private var towersTableName = "towers"
    private var attackersTableName = "attackers"
    private var heroesTableName = "heroes"

    private var idColumnName = "id"
    private var nameColumnName = "name"
    private var damageColumnName = "damage"
    private var rangeColumnName = "range"
    private var attackSpeedColumnName = "attack_speed"
    private var descriptionColumnName = "description"
    private var costColumnName = "cost"
    private var healthColumnName = "health"
    private var rewardColumnName = "reward"
    private var moveSpeedColumnName = "moveSpeed"
    private var skillsColumnName = "skills"





    override fun onCreate(db: SQLiteDatabase?) {
        val sql = "CREATE TABLE IF NOT EXISTS $towersTableName( $idColumnName int(11) NOT NULL PRIMARY KEY, $nameColumnName varchar(50), $damageColumnName int(11), $rangeColumnName float(11,5), $attackSpeedColumnName double(11,5), $descriptionColumnName varchar(255), $costColumnName int(11));"

        db!!.execSQL(sql)
        val sql2 = "CREATE TABLE IF NOT EXISTS $attackersTableName( $idColumnName int(11) NOT NULL PRIMARY KEY, $nameColumnName varchar(50), $damageColumnName int(11), $healthColumnName int(11), $moveSpeedColumnName  float(11,5), $descriptionColumnName varchar(255), $rewardColumnName int(11));"

        db.execSQL(sql2)

        val sql3 = "CREATE TABLE IF NOT EXISTS $heroesTableName( $idColumnName int(11) NOT NULL PRIMARY KEY, $nameColumnName varchar(50), $skillsColumnName varchar(255), $descriptionColumnName varchar(255));"

        db.execSQL(sql3)

        putTower(db, EyeTower())
        putTower(db, StoneTower())
        putTower(db, RookTower())
        putTower(db, AncientTower())
        putTower(db, FireTower())
        putTower(db, MageTower())
        putTower(db, GoldTower())
        putTower(db, ForestTower())
        putTower(db, HealthTower())
        putTower(db, GlassTower())
        putTower(db, IceTower())
        putTower(db, JapanTower())
        putTower(db, LogicTower())


        putAttacker(db, MobAttacker())
        putAttacker(db, WarriorAttacker())
        putAttacker(db, MageAttacker())
        putAttacker(db, CavalryAttacker())
        putAttacker(db, UFOAttacker())
        putAttacker(db, NinjaAttacker())
        putAttacker(db, SamuraiAttacker())
        putAttacker(db, AlienAttacker())
        putAttacker(db, AndroidAttacker())
        putAttacker(db, BlackDragonAttacker())
        putAttacker(db, GoblinAttacker())
        putAttacker(db, PhoenixAttacker())
        putAttacker(db, RunnerAttacker())
        putAttacker(db, SpiderAttacker())
        putAttacker(db, TankAttacker())
        putAttacker(db, TreantAttacker())
        putAttacker(db, UnicornAttacker())

        putHero(db, WarriorHero())
        putHero(db, EconomicHero())
        putHero(db, DefenderHero())
        putHero(db, MageHero())
        putHero(db, CommanderHero())

    }
    override fun onOpen(p0: SQLiteDatabase?) {

    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {

    }

    @SuppressLint("Recycle")
    fun getAllTowers() : ArrayList<TowerInformationStorage> {
        val db = readableDatabase
        val cur : Cursor = db.rawQuery("SELECT * FROM $towersTableName;", null)
        val array = ArrayList<TowerInformationStorage>()
        while(cur.moveToNext()){

            val id = cur.getInt(cur.getColumnIndex(idColumnName))
            val name = cur.getString(cur.getColumnIndex(nameColumnName))
            val damage = cur.getInt(cur.getColumnIndex(damageColumnName))
            val range = cur.getFloat(cur.getColumnIndex(rangeColumnName))
            val attackSpeed = cur.getDouble(cur.getColumnIndex(attackSpeedColumnName))
            val description = cur.getString(cur.getColumnIndex(descriptionColumnName))
            val cost = cur.getInt(cur.getColumnIndex(costColumnName))
            array.add(TowerInformationStorage(id, name, damage, range, attackSpeed, description, cost))
        }
        return array
    }
    @SuppressLint("Recycle")
    fun getAllAttackers() : ArrayList<AttackerInformationStorage> {
        val db = readableDatabase
        val cur : Cursor = db.rawQuery("SELECT * FROM $attackersTableName;", null)
        val array = ArrayList<AttackerInformationStorage>()
        while(cur.moveToNext()){

            val id = cur.getInt(cur.getColumnIndex(idColumnName))
            val name = cur.getString(cur.getColumnIndex(nameColumnName))
            val damage = cur.getInt(cur.getColumnIndex(damageColumnName))
            val health = cur.getInt(cur.getColumnIndex(healthColumnName))
            val moveSpeed = cur.getFloat(cur.getColumnIndex(moveSpeedColumnName))
            val description = cur.getString(cur.getColumnIndex(descriptionColumnName))
            val reward = cur.getInt(cur.getColumnIndex(rewardColumnName))
            array.add(AttackerInformationStorage(id, name, damage, health, moveSpeed, description, reward))
        }
        return array
    }

    @SuppressLint("Recycle")
    fun getAllHeroes() : ArrayList<HeroInformationStorage> {
        val db = readableDatabase
        val cur : Cursor = db.rawQuery("SELECT * FROM $heroesTableName;", null)
        val array = ArrayList<HeroInformationStorage>()
        while(cur.moveToNext()){

            val id = cur.getInt(cur.getColumnIndex(idColumnName))
            val name = cur.getString(cur.getColumnIndex(nameColumnName))
            val skills = cur.getString(cur.getColumnIndex(skillsColumnName))
            val description = cur.getString(cur.getColumnIndex(descriptionColumnName))
            array.add(HeroInformationStorage(id, name, skills, description))
        }
        return array
    }

    companion object {
        private const val DB_NAME = "info.db"
        private const val DB_VERSION = 1
    }

    private fun putTower(db : SQLiteDatabase, t : Tower){
        val contentV = ContentValues()
        contentV.put(idColumnName, t.id)
        contentV.put(nameColumnName, t.visibleName)
        contentV.put(damageColumnName, t.damage)
        contentV.put(rangeColumnName, t.range)
        contentV.put(attackSpeedColumnName, t.attackSpeed)
        contentV.put(descriptionColumnName, t.description)
        contentV.put(costColumnName, t.cost)

        db.insert(towersTableName, null, contentV)
    }

    private fun putAttacker(db : SQLiteDatabase, a : Attacker){
        val contentV = ContentValues()
        contentV.put(idColumnName, a.id)
        contentV.put(nameColumnName, a.visibleName)
        contentV.put(damageColumnName, a.damage)
        contentV.put(healthColumnName, a.health)
        contentV.put(moveSpeedColumnName, a.moveSpeed)
        contentV.put(descriptionColumnName, a.description)
        contentV.put(rewardColumnName, a.reward)

        db.insert(attackersTableName, null, contentV)
    }

    private fun putHero(db : SQLiteDatabase, h : Hero){
        val contentV = ContentValues()
        contentV.put(idColumnName, h.id)
        contentV.put(nameColumnName, h.name)
        contentV.put(descriptionColumnName, h.description)
        contentV.put(skillsColumnName, h.skills)

        db.insert(heroesTableName, null, contentV)
    }



}