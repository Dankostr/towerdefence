package com.example.towerdefence.data

import android.util.Log
import com.example.towerdefence.game.GameActivity
import com.example.towerdefence.game.gameobjects.attackers.Attacker
import com.example.towerdefence.game.userinterface.gameviewcanvas.GameMap
import java.io.BufferedReader
import java.io.InputStreamReader

object Save {
    fun loadMap(i: Int): GameMap {
        val stream = GameActivity.getMission(i)
        val read = InputStreamReader(stream)
        val reader = BufferedReader(read)

        val sizeX = Integer.parseInt(reader.readLine())
        val sizeY = Integer.parseInt(reader.readLine())

        Log.d("reader", "Created map $sizeX $sizeY")
        val map = GameMap(sizeX, sizeY)


        for (y in 0 until sizeY)
            for (x in 0 until sizeX) {
                val r = getChar(reader)
                map.map[y][x] = ObjectCreator.getMapObject(r)
            }

        reader.close()
        read.close()
        stream.close()

        return map
    }

    fun loadWave(i: Int): List<List<Attacker>> {
        val waves = ArrayList<ArrayList<Attacker>>()
        val stream = GameActivity.getWaves(i)

        var list = ArrayList<Attacker>()
        while (true) {
            val r = stream.read()
            if (r == -1) {
                stream.close()
                Log.d("debug", "returning ${waves.size} waves")
                return waves
            } else if (r.toChar() == '\n') {
                waves.add(list)
                Log.d("debug", "adding wave ${list.size}")
                list = ArrayList()
            } else if (r.toChar() != '\r')
                list.add(ObjectCreator.getAttacker(r))
        }
    }

    private fun getChar(reader : BufferedReader): Int {
        var r : Int
        do {
            r = reader.read()
        } while (r == '\r'.toInt() || r == '\n'.toInt())
        return r
    }
}