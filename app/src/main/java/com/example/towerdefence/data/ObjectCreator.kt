package com.example.towerdefence.data

import com.example.towerdefence.game.gameobjects.Base
import com.example.towerdefence.game.gameobjects.MapDrawable
import com.example.towerdefence.game.gameobjects.attackers.*
import com.example.towerdefence.game.gameobjects.flatobjects.*
import com.example.towerdefence.game.gameobjects.obstacles.*
import com.example.towerdefence.game.gameobjects.towers.Tower
import com.example.towerdefence.game.gameobjects.towers.laser_towers.*
import com.example.towerdefence.game.gameobjects.towers.projectile_towers.*

object ObjectCreator {

    fun getMapObject(m_id : Int) : MapDrawable {
        return when(m_id.toChar()) {
            '0' -> Grass()
            '1' -> Brick()
            '3' -> Gravel()
            '7' -> Rock()
            '8' -> Tree()
            '9' -> Base()
            else -> Grass()
        }
    }

    fun getTower(m_id : Int) : Tower {
        return when(m_id.toChar()) {
            'a' -> EyeTower()
            'b' -> StoneTower()
            'c' -> RookTower()
            'd' -> AncientTower()
            'e' -> FireTower()
            'f' -> MageTower()
            'g' -> GoldTower()
            'h' -> ForestTower()
            'i' -> HealthTower()
            'j' -> GlassTower()
            'k' -> IceTower()
            'l' -> JapanTower()
            'm' -> LogicTower()
            else -> EyeTower()
        }
    }
    
    fun getAttacker(m_id : Int) : Attacker {
        return when(m_id.toChar()) {
            'A' -> MobAttacker()
            'B' -> WarriorAttacker()
            'C' -> MageAttacker()
            'D' -> CavalryAttacker()
            'E' -> NinjaAttacker()
            'F' -> SamuraiAttacker()
            'G' -> UFOAttacker()
            'H' -> GoblinAttacker()
            'I' -> AndroidAttacker()
            'J' -> TankAttacker()
            'K' -> RunnerAttacker()
            'L' -> SnowmanAttacker()
            'M' -> SpiderAttacker()
            'N' -> PhoenixAttacker()
            'O' -> AlienAttacker()
            'P' -> UnicornAttacker()
            'Q' -> TreantAttacker()

            'R' -> BlackDragonAttacker() //boss
            else -> MobAttacker()
        }
    }
}