package com.example.towerdefence.game.gameobjects.attackers

import android.graphics.Bitmap
import com.example.towerdefence.data.ImageResources

class AlienAttacker : Attacker() {
    override var id: Int = 115
    override var damage: Int = 20
    override var description: String = "A long time ago in a galaxy far, far away..."
    override var health: Int = 150
    override var moveSpeed: Float = 0.02f
    override var reward: Int = 9
    override var visibleName: String = "Alien"

    override val image: Bitmap = ImageResources.getImage(id)
}