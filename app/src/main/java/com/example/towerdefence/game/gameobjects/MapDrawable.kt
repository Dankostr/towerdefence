package com.example.towerdefence.game.gameobjects

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Rect

abstract class MapDrawable {

    abstract var id: Int
    private val rect = Rect()
    abstract val image : Bitmap

    open fun draw(canvas: Canvas) {
        canvas.drawBitmap(image, null, rect, null)
    }

    open fun setPosition(x: Int, y: Int, width: Int, height: Int) {
        rect.set(x, y, x + width, y + height)
    }

    open fun getRect() : Rect {
        return rect
    }
}