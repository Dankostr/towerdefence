package com.example.towerdefence.game.gameobjects.attackers

import android.graphics.Bitmap
import com.example.towerdefence.data.ImageResources

class BlackDragonAttacker : Attacker() {
    override var id: Int = 191
    override var damage: Int = 10000
    override var description: String = "Your only way to defend yourself is to kill him"
    override var health: Int = 2500
    override var moveSpeed: Float = 0.02f
    override var reward: Int = 1000
    override var visibleName: String = "Black Dragon"

    override val image: Bitmap = ImageResources.getImage(id)
}