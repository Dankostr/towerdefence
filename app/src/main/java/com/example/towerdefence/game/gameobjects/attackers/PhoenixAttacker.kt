package com.example.towerdefence.game.gameobjects.attackers

import android.graphics.Bitmap
import android.util.Log
import com.example.towerdefence.data.ImageResources
import com.example.towerdefence.game.GameView
import kotlin.math.roundToInt

class PhoenixAttacker : Attacker() {
    override var id: Int = 114
    override var damage: Int = 50
    override var description: String = "YOLO doesn't apply to it"
    override var health: Int = 250
    override var moveSpeed: Float = 0.01f
    override var reward: Int = 50
    override var visibleName: String = "Phoenix"
    private var deathCounter = 0

    override val image: Bitmap = ImageResources.getImage(id)

    override fun die() {
        if (inGame) {
            if(deathCounter > 0) {
                inGame = false
                Log.d("die", "$goldPerMob")
                GameView.player.increaseGold((reward * goldMultiplier + goldPerMob).roundToInt())
            }
            else{
                health = 200
                deathCounter++
            }
        }
    }
}