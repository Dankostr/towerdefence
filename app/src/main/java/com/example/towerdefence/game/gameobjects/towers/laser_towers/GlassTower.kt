package com.example.towerdefence.game.gameobjects.towers.laser_towers

import android.graphics.Bitmap
import com.example.towerdefence.data.ImageResources

class GlassTower : LaserTower() {
    override var id: Int = 210
    override var attackSpeed: Double = 2.5
    override var cost: Int = 10
    override var damage: Int = 3
    override var description: String = "It can blind you"
    override var range: Float = 4f
    override var visibleName: String = "Glass Tower"

    override val image: Bitmap = ImageResources.getImage(id)

    override fun getInstance(): GlassTower {
        return GlassTower()
    }
}