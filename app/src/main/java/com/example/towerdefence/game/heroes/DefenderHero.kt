package com.example.towerdefence.game.heroes

import com.example.towerdefence.game.gameobjects.attackers.Attacker

class DefenderHero: Hero {

    override val name: String = "Defender"
    override val id: Int = 405
    override val description: String = "Defender knows how to minimize losses when facing his opponents"
    override val skills : String = "Defender's base receives 30% less damage from attackers"

    override fun setMultipliers() {
        Attacker.damageMultiplier = 0.7f
    }
}