package com.example.towerdefence.game.gameobjects.towers.laser_towers

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint
import com.example.towerdefence.data.ImageResources

class IceTower : LaserTower() {
    override var id: Int = 211
    override var attackSpeed: Double = 2.0
    override var cost: Int = 20
    override var damage: Int = 8
    override var description: String = "Let it go?"
    override var range: Float = 2f
    override var visibleName: String = "Ice Tower"
    override var laserColor = "light blue"

    override val image: Bitmap = ImageResources.getImage(id)

    override fun getInstance(): IceTower {
        return IceTower()
    }


}