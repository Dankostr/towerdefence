package com.example.towerdefence.game.userinterface.heroes

import android.graphics.Canvas
import android.view.MotionEvent
import com.example.towerdefence.game.userinterface.gameviewcanvas.GameViewCanvas

class HeroIconBar : GameViewCanvas {

    private var barTopLeftX: Int = 0
    private var barTopLeftY: Int = 0
    private var barWidth: Int = 0
    private var barHeight: Int = 0

    private val commanderHeroIcon = CommanderHeroIcon()
    private val warriorHeroIcon = WarriorHeroIcon()
    private val economicHeroIcon = EconomicHeroIcon()
    private val mageHeroIcon = MageHeroIcon()
    private val defenderHeroIcon = DefenderHeroIcon()

    override fun setPosition(topLeftX: Int, topLeftY: Int, width: Int, height: Int) {
        barTopLeftX = topLeftX
        barTopLeftY = topLeftY
        barWidth = width
        barHeight = height

        var spacingX = 0
        val iconSize = Math.min(barHeight, ((barWidth) * 0.20).toInt())

        warriorHeroIcon.setPosition(barTopLeftX, barTopLeftY, iconSize, iconSize)
        commanderHeroIcon.setPosition(barTopLeftX + iconSize + spacingX, barTopLeftY, iconSize, iconSize)
        economicHeroIcon.setPosition(barTopLeftX + 2 * iconSize + 2 * spacingX, barTopLeftY, iconSize, iconSize)
        mageHeroIcon.setPosition(barTopLeftX + 3 * iconSize + 3 * spacingX, barTopLeftY, iconSize, iconSize)
        defenderHeroIcon.setPosition(barTopLeftX + 4 * iconSize + 4 * spacingX, barTopLeftY, iconSize, iconSize)


    }

    override fun draw(canvas: Canvas) {
        warriorHeroIcon.draw(canvas)
        commanderHeroIcon.draw(canvas)
        economicHeroIcon.draw(canvas)
        mageHeroIcon.draw(canvas)
        defenderHeroIcon.draw(canvas)
    }

    override fun isInside(x: Int, y: Int): Boolean {
        return (x in (barTopLeftX..barTopLeftX + barWidth) &&
                y in (barTopLeftY..barTopLeftY + barHeight))
    }

    override fun onTouch(event: MotionEvent) {
        if (event.action == MotionEvent.ACTION_DOWN) {
            val x = event.x.toInt()
            val y = event.y.toInt()
            when {
                warriorHeroIcon.isInside(x, y) -> warriorHeroIcon.onTouch(event)
                commanderHeroIcon.isInside(x, y) -> commanderHeroIcon.onTouch(event)
                defenderHeroIcon.isInside(x, y) -> defenderHeroIcon.onTouch(event)
                economicHeroIcon.isInside(x, y) -> economicHeroIcon.onTouch(event)
                mageHeroIcon.isInside(x, y) -> mageHeroIcon.onTouch(event)
            }
        }
    }


}