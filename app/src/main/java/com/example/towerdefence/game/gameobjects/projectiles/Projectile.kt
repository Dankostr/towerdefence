package com.example.towerdefence.game.gameobjects.projectiles

import com.example.towerdefence.game.GameView
import com.example.towerdefence.game.gameobjects.MapDrawable
import com.example.towerdefence.game.gameobjects.attackers.Attacker
import kotlin.math.roundToInt

abstract class Projectile : MapDrawable() {

    private lateinit var targetedEnemy: Attacker
    private var speed: Float = 0f
    private var topLeftX: Int = 0
    private var topLeftY: Int = 0
    private var width: Int = 0
    private var height: Int = 0
    private var damage: Int = 0
    private var inGame: Boolean = true

    fun setTarget(enemy: Attacker) {
        this.targetedEnemy = enemy
    }

    override fun setPosition(x: Int, y: Int, width: Int, height: Int) {
        super.setPosition(x, y, width, height)
        this.topLeftX = x
        this.topLeftY = y
        this.width = width
        this.height = height
    }

    fun setSpeed(s: Float) {
        this.speed = s
    }

    fun setDamage(dmg: Int) {
        this.damage = dmg
    }

    fun isInGame() : Boolean {
        return inGame
    }

    fun physics() {
        val enemyPosition = targetedEnemy.getPosition()
        val enemyX = enemyPosition.x
        val enemyY = enemyPosition.y
        val selfX = topLeftX + width/2
        val selfY = topLeftY + height/2

        var dx = (enemyX - selfX) / GameView.map.mapElementWidth.toDouble()
        var dy = (enemyY - selfY) / GameView.map.mapElementHeight.toDouble()

        val distance = Math.sqrt(dx * dx + dy * dy)

        if (distance < speed) {
            targetedEnemy.getDamage(damage)
            inGame = false
        } else {
            val scale : Double = distance / speed
            dx /= scale
            dy /= scale

            topLeftX += (dx * GameView.map.mapElementWidth).roundToInt()
            topLeftY += (dy * GameView.map.mapElementHeight).roundToInt()
            update()
        }
    }

    private fun update() {
        super.setPosition(topLeftX, topLeftY, width, height)
    }
}