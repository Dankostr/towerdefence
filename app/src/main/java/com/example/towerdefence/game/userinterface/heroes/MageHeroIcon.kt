package com.example.towerdefence.game.userinterface.heroes

import com.example.towerdefence.data.ImageResources
import com.example.towerdefence.game.GameView
import com.example.towerdefence.game.heroes.MageHero

class MageHeroIcon : HeroIcon() {

    private val MAGE_HERO_BITMAP_ID = 404 // potrzebne Icony Bohaterów

    init {
        heroBitmap = ImageResources.getImage(MAGE_HERO_BITMAP_ID)
    }

    override fun applyHero() {
        GameView.player.setHero(MageHero())
        GameView.heroPortrait.setHeroBitmap(heroBitmap)
        MageHero().setMultipliers()
    }

}