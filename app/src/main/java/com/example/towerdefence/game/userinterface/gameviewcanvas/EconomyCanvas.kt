package com.example.towerdefence.game.userinterface.gameviewcanvas

import android.graphics.Canvas
import android.util.Log
import android.view.MotionEvent
import com.example.towerdefence.game.GameView
import com.example.towerdefence.game.userinterface.ConfirmationButton
import com.example.towerdefence.game.userinterface.upgradebars.GoldPerMobIncreaseBar
import com.example.towerdefence.game.userinterface.upgradebars.GoldPerWaveIncreaseBar


class EconomyCanvas : GameViewCanvas {
    
    private var canvasTopLeftX : Int = 0
    private var canvasTopLeftY : Int = 0
    private var canvasWidth : Int = 0
    private var canvasHeight : Int = 0

    private val goldPerMobIncreaseBar = GoldPerMobIncreaseBar()
    private val goldPerWaveIncreaseBar = GoldPerWaveIncreaseBar()
    private val confirmationButton = ConfirmationButton()

    override fun setPosition(topLeftX: Int, topLeftY: Int, width: Int, height: Int) {
        canvasTopLeftX = topLeftX
        canvasTopLeftY = topLeftY
        canvasWidth = width
        canvasHeight = height

        goldPerWaveIncreaseBar.setPosition(canvasTopLeftX, canvasTopLeftY + (canvasHeight * 0.05).toInt(),
            canvasWidth, (canvasHeight * 0.35).toInt())
        goldPerMobIncreaseBar.setPosition(canvasTopLeftX, canvasTopLeftY + (canvasHeight * 0.45).toInt(),
            canvasWidth, (canvasHeight * 0.35).toInt())
        confirmationButton.setPosition(canvasTopLeftX + canvasWidth/3,
            canvasTopLeftY + (canvasHeight *0.8).toInt(), canvasWidth/3, (canvasHeight *0.15).toInt())

    }
    
    override fun draw(canvas: Canvas) {
        //Log.d("draw", "Economy")
        goldPerMobIncreaseBar.draw(canvas)
        goldPerWaveIncreaseBar.draw(canvas)
        confirmationButton.draw(canvas)
    }

    override fun isInside(x : Int, y : Int) : Boolean {
        return (x in (canvasTopLeftX .. canvasTopLeftX + canvasWidth) &&
                y in (canvasTopLeftY .. canvasTopLeftY + canvasHeight))
    }

    override fun onTouch(event: MotionEvent) {
        if (event.action == MotionEvent.ACTION_DOWN) {
            val x = event.x.toInt()
            val y = event.y.toInt()
            when {
                goldPerMobIncreaseBar.isInside(x, y) -> goldPerMobIncreaseBar.onTouch(event)
                goldPerWaveIncreaseBar.isInside(x, y) -> goldPerWaveIncreaseBar.onTouch(event)
                confirmationButton.isInside(x, y) -> confirmationButton.onTouch(event)
            }
        }
    }
    
    
}