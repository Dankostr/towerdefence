package com.example.towerdefence.game.gameobjects.projectiles

import android.graphics.Bitmap
import com.example.towerdefence.data.ImageResources

class HeartProjectile : Projectile() {
    override var id: Int = 303
    override val image: Bitmap = ImageResources.getImage(id)

    init {
        setSpeed(0.1f)
    }
}