package com.example.towerdefence.game.heroes

interface Hero {

    val name : String
    val id : Int
    val description : String
    val skills : String

    fun setMultipliers()

}