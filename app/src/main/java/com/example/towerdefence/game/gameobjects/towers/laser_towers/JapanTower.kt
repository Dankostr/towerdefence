package com.example.towerdefence.game.gameobjects.towers.laser_towers

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint
import com.example.towerdefence.data.ImageResources

//Japanese, a nie Japan...
class JapanTower : LaserTower() {
    override var id: Int = 212
    override var attackSpeed: Double = 2.0
    override var cost: Int = 40
    override var damage: Int = 16
    override var description: String = "日本国（にほんこく、にっぽんこく）、または日本（にほん、にっぽん）は、日本列島（北海道・本州・四国・九州の主要四島およびそれに付随する島々）および南西諸島・伊豆諸島・小笠原諸島などからなる東アジアの島国[1][2]。議会制民主主義国家である。首都は東京都。"
    override var range: Float = 4f
    override var visibleName: String = "Japanese Tower"
    override var laserColor = "pink"

    override val image: Bitmap = ImageResources.getImage(id)

    override fun getInstance(): JapanTower {
        return JapanTower()
    }


}