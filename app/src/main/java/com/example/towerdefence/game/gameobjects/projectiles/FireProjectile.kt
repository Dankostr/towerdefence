package com.example.towerdefence.game.gameobjects.projectiles

import android.graphics.Bitmap
import com.example.towerdefence.data.ImageResources

class FireProjectile : Projectile() {

        override var id: Int = 305
        override val image: Bitmap = ImageResources.getImage(id)

        init {
            setSpeed(0.3f)
        }

}