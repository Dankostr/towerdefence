package com.example.towerdefence.game.userinterface.upgradebars

import com.example.towerdefence.data.ImageResources
import com.example.towerdefence.game.GameView
import com.example.towerdefence.game.gameobjects.Player

class BaseShieldIncreaseBar : UpgradeBar() {

    private val TOWER_DAMAGE_BITMAP_ID = 902

    init {
        buttonBitmap = ImageResources.getImage(INCREASE_BITMAP_ID)
        iconBitmap = ImageResources.getImage(TOWER_DAMAGE_BITMAP_ID)
        firstLine = "Increases base hit points by ${Player.BASE_SHIELD_PER_LEVEL} points"
        secondLine = "Current bonus: ${GameView.player.getBaseShieldLevel() * Player.BASE_SHIELD_PER_LEVEL} hp"
        costLine = "Cost: ${GameView.player.getBaseShieldLevel() * 50 + 25} gold"
    }

    override fun increasePlayersLevel() {
        GameView.player.upgradeBaseShield()
    }

    override fun calculateCost(): Int {
        secondLine = "Current bonus: ${GameView.player.getBaseShieldLevel() * Player.BASE_SHIELD_PER_LEVEL} hp"
        costLine = "Cost: ${GameView.player.getBaseShieldLevel() * 50 + 25} gold"
        return GameView.player.getBaseShieldLevel() * 50 + 25
    }
}