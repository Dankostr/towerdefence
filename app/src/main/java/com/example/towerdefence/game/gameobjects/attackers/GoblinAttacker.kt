package com.example.towerdefence.game.gameobjects.attackers

import android.graphics.Bitmap
import com.example.towerdefence.data.ImageResources

class GoblinAttacker : Attacker() {
    override var id: Int = 107
    override var damage: Int = 4
    override var description: String = "A single goblin doesn't present a serious threat to your base, however in " +
            "larger groups they can be dangerous"
    override var health: Int = 20
    override var moveSpeed: Float = 0.0175f
    override var reward: Int = 3
    override var visibleName: String = "Goblin"

    override val image: Bitmap = ImageResources.getImage(id)
}