package com.example.towerdefence.game.gameobjects.towers.projectile_towers

import android.graphics.Bitmap
import com.example.towerdefence.data.ImageResources
import com.example.towerdefence.game.gameobjects.projectiles.FireProjectile
import com.example.towerdefence.game.gameobjects.projectiles.Projectile

class FireTower : ProjectileTower() {
    override var id: Int = 205
    override var attackSpeed: Double = 1.5
    override var cost: Int = 30
    override var damage: Int = 30
    override var description: String = "It's lit"
    override var range: Float = 1.6f
    override var visibleName: String = "Fire Tower"

    override val image: Bitmap = ImageResources.getImage(id)

    override fun createProjectile() : Projectile {
        return FireProjectile()
    }

    override fun getInstance(): FireTower {
        return FireTower()
    }
}