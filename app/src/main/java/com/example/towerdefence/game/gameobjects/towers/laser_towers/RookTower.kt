package com.example.towerdefence.game.gameobjects.towers.laser_towers

import android.graphics.Bitmap
import com.example.towerdefence.data.ImageResources

class RookTower : LaserTower() {
    override var id: Int = 203
    override var attackSpeed: Double = 1.0
    override var cost: Int = 30
    override var damage: Int = 20
    override var description: String = "I have the impression I've already seen this tower in a different game..."
    override var range: Float = 3f
    override var visibleName: String = "Rook"

    override val image: Bitmap = ImageResources.getImage(id)

    override fun getInstance(): RookTower {
        return RookTower()
    }
}