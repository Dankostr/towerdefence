package com.example.towerdefence.game.gameobjects.towers.projectile_towers

import android.graphics.Bitmap
import com.example.towerdefence.data.ImageResources
import com.example.towerdefence.game.gameobjects.projectiles.Projectile
import com.example.towerdefence.game.gameobjects.projectiles.StoneProjectile

class StoneTower : ProjectileTower() {
    override var id: Int = 202
    override var attackSpeed: Double = 0.35
    override var cost: Int = 18
    override var damage: Int = 25
    override var description: String = "It throws stones at its opponents"
    override var range: Float = 2f
    override var visibleName: String = "Stone Tower"

    override val image: Bitmap = ImageResources.getImage(id)

    override fun createProjectile() : Projectile {
        return StoneProjectile()
    }

    override fun getInstance(): StoneTower {
        return StoneTower()
    }
}