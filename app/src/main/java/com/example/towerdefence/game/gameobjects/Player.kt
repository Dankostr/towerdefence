package com.example.towerdefence.game.gameobjects

import com.example.towerdefence.game.GameView
import com.example.towerdefence.game.gameobjects.attackers.Attacker
import com.example.towerdefence.game.gameobjects.towers.Tower
import com.example.towerdefence.game.heroes.Hero

class Player {

    companion object {
        const val TOWER_DAMAGE_PER_LEVEL = 10
        const val BASE_SHIELD_PER_LEVEL = 40
        const val GOLD_PER_MOB_PER_LEVEL = 1
        const val GOLD_PER_WAVE_PER_LEVEL = 10

        const val GOLD_PER_WAVE = 10
    }

    private var gold: Int = 0
    private var baseHp: Int = 0
    private lateinit var hero: Hero

    private var towerDamageUpgradeLevel: Int = 0
    private var baseShieldUpgradeLevel: Int = 0
    private var goldPerMobUpgradeLevel: Int = 0
    private var goldPerWaveUpgradeLevel: Int = 0

    fun setDefaultValues() {
        this.gold = 50
        this.baseHp = 100
        this.towerDamageUpgradeLevel = 0
        this.baseShieldUpgradeLevel = 0
        this.goldPerMobUpgradeLevel = 0
        this.goldPerWaveUpgradeLevel = 0

    }

    fun setGold(gold: Int) {
        this.gold = gold
    }

    fun increaseGold(gold: Int) {
        this.gold += gold
    }

    fun decreaseGold(gold: Int) {
        this.gold = Math.max(0, this.gold - gold)
    }

    fun getGold() : Int {
        return gold
    }

    fun setHp(hp: Int) {
        this.baseHp = hp
    }

    fun increaseHp(hp: Int) {
        this.baseHp += hp
    }

    fun decreaseHp(hp: Int) {
        this.baseHp = Math.max(0, this.baseHp - hp)
        if(this.baseHp == 0) {
            GameView.isLost = true
        }
    }

    fun getHp() : Int {
        return baseHp
    }

    fun upgradeGoldPerMob() {
        this.goldPerMobUpgradeLevel++
        Attacker.goldPerMob += (GOLD_PER_MOB_PER_LEVEL)
    }

    fun getGoldPerMobLevel() : Int {
        return this.goldPerMobUpgradeLevel
    }

    fun upgradeGoldPerWave() {
        this.goldPerWaveUpgradeLevel++
    }

    fun getGoldPerWaveLevel() : Int {
        return this.goldPerWaveUpgradeLevel
    }

    fun upgradeTowerDamage() {
        this.towerDamageUpgradeLevel++
        Tower.damageMultiplier += (TOWER_DAMAGE_PER_LEVEL.toFloat() / 100)
    }

    fun getTowerDamageLevel() : Int {
        return this.towerDamageUpgradeLevel
    }

    fun upgradeBaseShield() {
        this.baseShieldUpgradeLevel++
        increaseHp(BASE_SHIELD_PER_LEVEL)
    }

    fun getBaseShieldLevel() : Int {
        return this.baseShieldUpgradeLevel
    }

    fun setHero(hero: Hero) {
        this.hero = hero
    }

    fun getHero() : Hero {
        return this.hero
    }
}