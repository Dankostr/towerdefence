package com.example.towerdefence.game.gameobjects.towers

import android.graphics.Canvas
import com.example.towerdefence.game.gameobjects.attackers.Attacker

interface BaseTower {
    fun checkRange(attackers: List<Attacker>)
    fun attack()
}