package com.example.towerdefence.game.gameobjects.towers.laser_towers

import android.graphics.Bitmap
import com.example.towerdefence.data.ImageResources

class EyeTower : LaserTower() {
    override var id: Int = 201
    override var attackSpeed: Double = 4.0
    override var cost: Int = 10
    override var damage: Int = 2
    override var description: String = "It's watching you"
    override var range: Float = 2f
    override var visibleName: String = "Eye Tower"

    override val image: Bitmap = ImageResources.getImage(id)

    override fun getInstance(): EyeTower {
        return EyeTower()
    }
}