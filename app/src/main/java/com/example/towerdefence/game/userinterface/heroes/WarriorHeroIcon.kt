package com.example.towerdefence.game.userinterface.heroes

import com.example.towerdefence.data.ImageResources
import com.example.towerdefence.game.GameView
import com.example.towerdefence.game.heroes.WarriorHero


class WarriorHeroIcon : HeroIcon() {

    private val WARRIOR_HERO_BITMAP_ID = 401 // potrzebne Icony Bohaterów

    init {
        heroBitmap = ImageResources.getImage(WARRIOR_HERO_BITMAP_ID)
    }

    override fun applyHero() {
        GameView.player.setHero(WarriorHero())
        GameView.heroPortrait.setHeroBitmap(heroBitmap)
        WarriorHero().setMultipliers()
    }

}