package com.example.towerdefence.game.gameobjects.towers.projectile_towers

import android.graphics.Bitmap
import com.example.towerdefence.data.ImageResources
import com.example.towerdefence.game.GameView
import com.example.towerdefence.game.gameobjects.projectiles.GoldProjectile
import com.example.towerdefence.game.gameobjects.projectiles.Projectile
import com.example.towerdefence.game.gameobjects.towers.laser_towers.LaserTower
import kotlin.math.roundToInt

class GoldTower : ProjectileTower() {
    override var id: Int = 207
    override var attackSpeed: Double = 2.0
    override var cost: Int = 80
    override var damage: Int = 50
    override var description: String = "You must pay a lot for real power"
    override var range: Float = 3f
    override var visibleName: String = "Gold Tower"

    override val image: Bitmap = ImageResources.getImage(id)

    override fun createProjectile() : Projectile {
        return GoldProjectile()
    }

    override fun getInstance(): GoldTower {
        return GoldTower()
    }

    override fun attack() {
        val r = getRect()
        var dmg = damage

        if (GameView.player.getGold() >= 2) {
            GameView.player.decreaseGold(2)
            dmg += 10
        }

        val projectile = createProjectile().apply {
            this.setDamage((dmg * damageMultiplier).roundToInt())
            this.setTarget(shotAttacker!!)
            this.setPosition(r.left, r.top, r.width() / 3, r.height() / 3)
        }
        GameView.map.addProjectile(projectile)
        frames = 0.0
    }

}