package com.example.towerdefence.game.userinterface.gameviewcanvas

import android.graphics.Canvas
import android.view.MotionEvent

interface GameViewCanvas {

    fun setPosition(topLeftX: Int, topLeftY: Int, width: Int, height: Int)

    fun draw(canvas: Canvas)

    fun isInside(x : Int, y : Int) : Boolean

    fun onTouch(event: MotionEvent)
}