package com.example.towerdefence.game.gameobjects.attackers

import android.graphics.Bitmap
import com.example.towerdefence.data.ImageResources

class AndroidAttacker : Attacker() {
    override var id: Int = 108
    override var damage: Int = 30
    override var description: String = "Its main opponent grows on trees..."
    override var health: Int = 75
    override var moveSpeed: Float = 0.01f
    override var reward: Int = 7
    override var visibleName: String = "AnDroid"

    override val image: Bitmap = ImageResources.getImage(id)
}