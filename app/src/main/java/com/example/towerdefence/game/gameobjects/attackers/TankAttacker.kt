package com.example.towerdefence.game.gameobjects.attackers

import android.graphics.Bitmap
import com.example.towerdefence.data.ImageResources

class TankAttacker : Attacker() {
    override var id: Int = 109
    override var damage: Int = 30
    override var description: String = "This machine seems to be invincible"
    override var health: Int = 350
    override var moveSpeed: Float = 0.01f
    override var reward: Int = 17
    override var visibleName: String = "Tank"

    override val image: Bitmap = ImageResources.getImage(id)
}