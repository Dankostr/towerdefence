package com.example.towerdefence.game.gameobjects.attackers


import android.graphics.Bitmap
import com.example.towerdefence.data.ImageResources

class MageAttacker : Attacker() {
    override var id: Int = 102
    override var damage: Int = 15
    override var description: String = "Fus Ro Dah!"
    override var health: Int = 40
    override var moveSpeed: Float = 0.01f
    override var reward: Int = 5
    override var visibleName: String = "Mage"

    override val image: Bitmap = ImageResources.getImage(id)
}