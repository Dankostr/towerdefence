package com.example.towerdefence.game.userinterface.upgradebars

import com.example.towerdefence.data.ImageResources
import com.example.towerdefence.game.GameView
import com.example.towerdefence.game.gameobjects.Player

class GoldPerMobIncreaseBar : UpgradeBar() {

    private val GOLD_INCREASE_BITMAP_ID = 901

    init {
        buttonBitmap = ImageResources.getImage(INCREASE_BITMAP_ID)
        iconBitmap = ImageResources.getImage(GOLD_INCREASE_BITMAP_ID)
        firstLine = "Increases gold received per enemy by ${Player.GOLD_PER_MOB_PER_LEVEL}"
        secondLine = "Current bonus: ${GameView.player.getGoldPerMobLevel() * Player.GOLD_PER_MOB_PER_LEVEL} gold"
        costLine = "Cost: ${GameView.player.getGoldPerMobLevel() * 15 + 5} gold"
    }

    override fun increasePlayersLevel() {
        GameView.player.upgradeGoldPerMob()
    }

    override fun calculateCost(): Int {
        secondLine = "Current bonus: ${GameView.player.getGoldPerMobLevel() * Player.GOLD_PER_MOB_PER_LEVEL} gold"
        costLine = "Cost: ${GameView.player.getGoldPerMobLevel() * 15 + 5} gold"
        return GameView.player.getGoldPerMobLevel() * 15 + 5
    }

}