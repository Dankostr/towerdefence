package com.example.towerdefence.game.gameobjects.towers.projectile_towers

import android.graphics.Bitmap
import com.example.towerdefence.data.ImageResources
import com.example.towerdefence.game.gameobjects.projectiles.ForestProjectile
import com.example.towerdefence.game.gameobjects.projectiles.Projectile
import com.example.towerdefence.game.gameobjects.towers.laser_towers.LaserTower

class ForestTower : ProjectileTower() {
    override var id: Int = 208
    override var attackSpeed: Double = 3.0
    override var cost: Int = 10
    override var damage: Int = 2
    override var description: String = "Green power. Death to the enemies of nature"
    override var range: Float = 2f
    override var visibleName: String = "Forest Tower"

    override val image: Bitmap = ImageResources.getImage(id)

    override fun createProjectile() : Projectile {
        return ForestProjectile()
    }

    override fun getInstance(): ForestTower {
        return ForestTower()
    }
}