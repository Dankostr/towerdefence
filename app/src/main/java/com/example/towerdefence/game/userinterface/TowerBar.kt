package com.example.towerdefence.game.userinterface

import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import android.util.Log
import android.view.MotionEvent
import com.example.towerdefence.game.GameView
import com.example.towerdefence.game.gameobjects.towers.*
import com.example.towerdefence.game.gameobjects.towers.laser_towers.*
import com.example.towerdefence.game.gameobjects.towers.projectile_towers.*

class TowerBar {

    private var barTopLeftX : Int = 0
    private var barTopLeftY : Int = 0
    private var barWidth : Int = 0
    private var barHeight : Int = 0
    private var itemWidth : Int = 0
    private var itemHeight : Int = 0

    private val borderWidth : Int = 2
    private val towerHeightScale = 0.8f

    private var towerArrayList: ArrayList<Tower?>
    private var barSize: Int = 4

    init {
        towerArrayList = ArrayList()
        for (i in 0 until barSize) {
            towerArrayList.add(null)
        }
    }

    fun setRandomTowers(count : Int) {
        assert(count in 1..7)
        val tmpArrayList = ArrayList<Tower?>()

        tmpArrayList.add(StoneTower())
        tmpArrayList.add(EyeTower())
        tmpArrayList.add(RookTower())
        tmpArrayList.add(AncientTower())
        tmpArrayList.add(FireTower())
        tmpArrayList.add(MageTower())
        tmpArrayList.add(GoldTower())
        tmpArrayList.add(ForestTower())
        tmpArrayList.add(HealthTower())
        tmpArrayList.add(GlassTower())
        tmpArrayList.add(IceTower())
        tmpArrayList.add(JapanTower())
        tmpArrayList.add(LogicTower())

        tmpArrayList.shuffle()

        val finalArrayList = ArrayList<Tower?>()

        for(i in 0 until count) {
            finalArrayList.add(tmpArrayList[i])
        }
        setTowers(finalArrayList)
    }

    fun setRandomCheapTowers(count: Int) {
        assert(count in 1..6)

        val tmpArrayList = ArrayList<Tower?>()

        tmpArrayList.add(StoneTower())
        tmpArrayList.add(EyeTower())
        tmpArrayList.add(ForestTower())
        tmpArrayList.add(HealthTower())
        tmpArrayList.add(GlassTower())
        tmpArrayList.add(LogicTower())

        tmpArrayList.shuffle()

        val finalArrayList = ArrayList<Tower?>()

        for (i in 0 until count) {
            finalArrayList.add(tmpArrayList[i])
        }
        setTowers(finalArrayList)
    }

    fun setTowers(towers: ArrayList<Tower?>) {
        barSize = towers.size
        towerArrayList = towers
        calcItemSize()
        updateTowers()
    }

    fun getTower(index: Int) : Tower? {
        return towerArrayList[index]
    }

    fun setPosition(topLeftX: Int, topLeftY: Int, width: Int, height: Int) {
        barTopLeftX = topLeftX
        barTopLeftY = topLeftY
        barWidth = width
        barHeight = height

        calcItemSize()
        updateTowers()
    }

    fun draw(canvas: Canvas) {
        drawRectangle(canvas)
        var startingX = barTopLeftX + borderWidth * 2
        val startingY = barTopLeftY + borderWidth
        for (i in 0 until barSize) {
            drawTower(startingX, startingY, itemWidth, itemHeight, i, canvas)
            startingX += (itemWidth + borderWidth)
        }
    }

    fun isInside(x : Int, y : Int) : Boolean {
        return (x in (barTopLeftX .. barTopLeftX + barWidth) &&
                y in (barTopLeftY .. barTopLeftY + barHeight))
    }

    fun onTouch(event: MotionEvent) {
        Log.d("Touch", "Bar")
        if (event.action == MotionEvent.ACTION_DOWN) {
            val x = event.x.toInt()
            val y = event.y.toInt()
            val i = (x - barTopLeftX) * barSize / barWidth

            if (towerArrayList[i] != null) {
                val tower = towerArrayList[i]!!.getInstance()
                val w = GameView.map.getElementWidth()
                val h = GameView.map.getElementHeight()
                tower.setPosition(
                    x - w / 2, y - h / 2, w, h)
                GameView.selectedTower = tower
            }
        }
    }

    private fun updateTowers() {
        var startingX = barTopLeftX + borderWidth
        val startingY = barTopLeftY + borderWidth
        for (i in 0 until barSize) {
            setTowerPosition(startingX, startingY, itemWidth, itemHeight, i)
            startingX += (itemWidth + borderWidth)
        }
    }

    private fun calcItemSize() {
        itemWidth = (barWidth - (barSize+1) * borderWidth) / barSize
        itemHeight = (barHeight - 2 * borderWidth)
    }

    private fun drawRectangle(canvas: Canvas) {
        val paint = Paint()
        paint.color = Color.rgb(96, 96, 96)
        canvas.drawRect(Rect().apply {
            this.set(barTopLeftX, barTopLeftY, barTopLeftX+barWidth, barTopLeftY+barHeight) }, paint)
    }

    private fun setTowerPosition(topLeftX: Int, topLeftY: Int, width: Int, height: Int, index: Int) {
        if (towerArrayList[index] != null) {
            val towerHeight = (height * towerHeightScale).toInt()
            towerArrayList[index]!!.setPosition(
                topLeftX, topLeftY + towerHeight / 2, width, towerHeight.toInt() / 2)
        }
    }

    private fun drawTower(topLeftX: Int, topLeftY: Int, width: Int, height: Int, index: Int, canvas: Canvas) {
        val towerHeight = height * towerHeightScale
        if (towerArrayList[index] != null) {
            towerArrayList[index]!!.draw(canvas)
            val paint = Paint().apply { this.color = Color.GREEN; this.textSize = 18f }
            canvas.drawText(towerArrayList[index]!!.cost.toString() + " $", topLeftX + width * 0.35f,
                topLeftY + towerHeight + 18, paint)
        }
    }

}