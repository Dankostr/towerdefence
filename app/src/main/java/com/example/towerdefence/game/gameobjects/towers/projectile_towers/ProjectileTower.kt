package com.example.towerdefence.game.gameobjects.towers.projectile_towers

import android.graphics.Canvas
import android.util.Log
import com.example.towerdefence.game.GameView
import com.example.towerdefence.game.gameobjects.projectiles.Projectile
import com.example.towerdefence.game.gameobjects.towers.Tower
import kotlin.math.roundToInt

abstract class ProjectileTower : Tower() {

    override fun drawAttack(canvas: Canvas) {}

    override fun attack() {
        val r = getRect()
        val projectile = createProjectile().apply {
            this.setDamage((damage * damageMultiplier).roundToInt())
            this.setTarget(shotAttacker!!)
            this.setPosition(r.left, r.top, r.width() / 3, r.height() / 3)
        }
        GameView.map.addProjectile(projectile)
        frames = 0.0
    }

    abstract fun createProjectile() : Projectile
}