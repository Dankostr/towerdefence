package com.example.towerdefence.game.gameobjects.towers.laser_towers

import android.graphics.Bitmap
import com.example.towerdefence.data.ImageResources

class AncientTower : LaserTower() {
    override var id: Int = 204
    override var attackSpeed: Double = 2.5
    override var cost: Int = 20
    override var damage: Int = 5
    override var description: String = "A long long time ago..."
    override var range: Float = 4f
    override var visibleName: String = "Ancient Tower"

    override val image: Bitmap = ImageResources.getImage(id)

    override fun getInstance(): AncientTower {
        return AncientTower()
    }
}