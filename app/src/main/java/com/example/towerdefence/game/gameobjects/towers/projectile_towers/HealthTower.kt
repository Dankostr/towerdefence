package com.example.towerdefence.game.gameobjects.towers.projectile_towers

import android.graphics.Bitmap
import com.example.towerdefence.data.ImageResources
import com.example.towerdefence.game.GameView
import com.example.towerdefence.game.gameobjects.projectiles.HeartProjectile
import com.example.towerdefence.game.gameobjects.projectiles.Projectile
import kotlin.math.roundToInt

class HealthTower : ProjectileTower() {
    override var id: Int = 209
    override var attackSpeed: Double = 0.5
    override var cost: Int = 15
    override var damage: Int = 20
    override var description: String = "It can heal the main base"
    override var range: Float = 2.2f
    override var visibleName: String = "Health Tower"

    var healCycle = 3
    var shootCount = 0
    var healPower = 2

    override val image: Bitmap = ImageResources.getImage(id)

    override fun createProjectile() : Projectile {
        return HeartProjectile()
    }

    override fun attack() {
        val r = getRect()
        val projectile = createProjectile().apply {
            this.setDamage((damage * damageMultiplier).roundToInt())
            this.setTarget(shotAttacker!!)
            this.setPosition(r.left, r.top, r.width() / 3, r.height() / 3)
        }
        GameView.map.addProjectile(projectile)
        shootCount++
        if(shootCount >= healCycle){
            GameView.player.increaseHp(healPower)
            shootCount = 0
        }
        frames = 0.0
    }

    override fun getInstance(): HealthTower {
        return HealthTower()
    }
}