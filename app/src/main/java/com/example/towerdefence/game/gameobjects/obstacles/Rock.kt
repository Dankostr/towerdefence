package com.example.towerdefence.game.gameobjects.obstacles

import android.graphics.Bitmap
import android.graphics.Canvas
import com.example.towerdefence.data.ImageResources
import com.example.towerdefence.game.gameobjects.MapDrawable

class Rock : MapDrawable() {
    override var id: Int = 21
    override val image: Bitmap = ImageResources.getImage(id)

    private var background = ImageResources.getDefaultBackground()

    override fun draw(canvas: Canvas) {
        canvas.drawBitmap(background, null, getRect(), null)
        super.draw(canvas)
    }

}