package com.example.towerdefence.game.heroes

import com.example.towerdefence.game.gameobjects.towers.Tower

class CommanderHero : Hero {
    override val name: String = "Commander"
    override val id: Int = 402
    override val description: String = "Commander is an experienced fighter who knows how to increase soldiers' morale"
    override val skills : String = "Towers owned by this hero have their attack speed increased by 20%"
    override fun setMultipliers() {
        Tower.speedMultiplier = 1.2f
    }
}