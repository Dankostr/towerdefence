package com.example.towerdefence.game.userinterface

import android.graphics.*
import android.util.Log
import android.view.MotionEvent
import com.example.towerdefence.data.ImageResources
import com.example.towerdefence.game.GameView

class TopButtonsBar(private val view : GameView) {

    private val UPGRADE_BITMAP_ID = 903
    private val ECONOMY_BITMAP_ID = 904
    private val REROLL_BITMAP_ID = 905

    private var barTopLeftX : Int = 0
    private var barTopLeftY : Int = 0
    private var barWidth : Int = 0
    private var barHeight : Int = 0

    private val upgradeRect = Rect()
    private val economyRect = Rect()
    private val rerollRect = Rect()
    private val rerollCostRect = Rect()

    private val upgradeClickRect = Rect()
    private val economyClickRect = Rect()
    private val rerollClickRect = Rect()

    private val upgradeImage : Bitmap = ImageResources.getImage(UPGRADE_BITMAP_ID)
    private val economyImage : Bitmap = ImageResources.getImage(ECONOMY_BITMAP_ID)
    private val rerollImage : Bitmap = ImageResources.getImage(REROLL_BITMAP_ID)

    private val rerollCostText = "10$"

    fun setPosition(topLeftX: Int, topLeftY: Int, width: Int, height: Int) {
        barTopLeftX = topLeftX
        barTopLeftY = topLeftY
        barWidth = width
        barHeight = height

        val spacingX = (barWidth * 0.05).toInt()
        val buttonWidth = ((barWidth * 0.8) / 3).toInt()
        val spacingY = (barHeight * 0.1).toInt()
        val buttonHeight = (barHeight * 0.8).toInt()

        upgradeClickRect.set(barTopLeftX + spacingX, spacingY, barTopLeftX + spacingX + buttonWidth, spacingY + buttonHeight)
        economyClickRect.set(barTopLeftX + 2*spacingX + buttonWidth, spacingY,  barTopLeftX + 2*spacingX + 2*buttonWidth, spacingY + buttonHeight)
        rerollClickRect.set(barTopLeftX + 3*spacingX + 2*buttonWidth, spacingY, barTopLeftX + 3*spacingX + 3*buttonWidth, spacingY + buttonHeight)

        upgradeRect.set(upgradeClickRect.left + (upgradeClickRect.width() - upgradeClickRect.height())/2,
            upgradeClickRect.top,
            upgradeClickRect.left + (upgradeClickRect.width() - upgradeClickRect.height())/2 + upgradeClickRect.height(),
            upgradeClickRect.top + upgradeClickRect.height())
        economyRect.set(economyClickRect.left + (economyClickRect.width() - economyClickRect.height())/2,
            economyClickRect.top,
            economyClickRect.left + (economyClickRect.width() - economyClickRect.height())/2 + economyClickRect.height(),
            economyClickRect.top + economyClickRect.height())
        rerollRect.set(rerollClickRect.left + (rerollClickRect.width() - rerollClickRect.height())/2,
            rerollClickRect.top,
            rerollClickRect.left + (rerollClickRect.width() - rerollClickRect.height())/2 + rerollClickRect.height(),
            rerollClickRect.top + rerollClickRect.height())
        rerollCostRect.set(rerollRect.right,
            rerollRect.bottom,
            rerollRect.width() + rerollRect.right,
            rerollRect.height() + rerollRect.bottom)
    }

    fun draw(canvas: Canvas) {
        drawRectangle(canvas)
        drawButtons(canvas)
        drawCost(canvas)
    }

    private fun drawRectangle(canvas: Canvas) {
        val paint = Paint()
        paint.color = Color.rgb(96, 96, 96)
        canvas.drawRect(Rect().apply {
            this.set(barTopLeftX, barTopLeftY, barTopLeftX+barWidth, barTopLeftY+barHeight) }, paint)
    }

    private fun drawButtons(canvas: Canvas) {
        canvas.drawBitmap(upgradeImage, null, upgradeRect, null)
        canvas.drawBitmap(economyImage, null, economyRect, null)
        canvas.drawBitmap(rerollImage, null, rerollRect, null)
    }

    private fun drawCost(canvas: Canvas) {
        val paint = Paint()
        paint.textSize = 20f
        paint.color = Color.GREEN
        canvas.drawText(rerollCostText, rerollCostRect.left.toFloat(), rerollCostRect.top.toFloat(), paint)
    }

    fun isInside(x : Int, y : Int) : Boolean {
        return (x in (barTopLeftX .. barTopLeftX + barWidth) &&
                y in (barTopLeftY .. barTopLeftY + barHeight))
    }

    fun onTouch(event: MotionEvent) {
        if (event.action == MotionEvent.ACTION_DOWN) {
            val x = event.x.toInt()
            val y = event.y.toInt()

            when {
                upgradeClickRect.contains(x, y) -> upgradeClicked()
                economyClickRect.contains(x, y) -> economyClicked()
                rerollClickRect.contains(x, y) -> rerollClicked()
            }
        }
    }

    private fun upgradeClicked() {
        if (GameView.currentCanvas == GameView.CurrentCanvas.UPGRADE) {
            GameView.currentCanvas = GameView.CurrentCanvas.MAP
        } else {
            GameView.currentCanvas = GameView.CurrentCanvas.UPGRADE
        }
    }

    private fun economyClicked() {
        if (GameView.currentCanvas == GameView.CurrentCanvas.ECONOMY) {
            GameView.currentCanvas = GameView.CurrentCanvas.MAP
        } else {
            GameView.currentCanvas = GameView.CurrentCanvas.ECONOMY
        }
    }

    private fun rerollClicked() {
        if (GameView.player.getGold() >= 10) {
            GameView.player.decreaseGold(10)
            view.setRandomTowers()
        }
    }

}