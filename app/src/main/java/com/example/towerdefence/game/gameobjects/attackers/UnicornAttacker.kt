package com.example.towerdefence.game.gameobjects.attackers

import android.graphics.Bitmap
import com.example.towerdefence.data.ImageResources

class UnicornAttacker : Attacker() {
    override var id: Int = 116
    override var damage: Int = 75
    override var description: String = "A magical unicorn :D"
    override var health: Int = 300
    override var moveSpeed: Float = 0.03f
    override var reward: Int = 30
    override var visibleName: String = "Unicorn"

    override val image: Bitmap = ImageResources.getImage(id)
}