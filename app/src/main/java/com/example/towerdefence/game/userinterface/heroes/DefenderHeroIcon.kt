package com.example.towerdefence.game.userinterface.heroes

import com.example.towerdefence.data.ImageResources
import com.example.towerdefence.game.GameView
import com.example.towerdefence.game.heroes.DefenderHero

class DefenderHeroIcon : HeroIcon() {

    private val DEFENDER_HERO_BITMAP_ID = 405 // potrzebne Icony Bohaterów

    init {
        heroBitmap = ImageResources.getImage(DEFENDER_HERO_BITMAP_ID)
    }

    override fun applyHero() {
        GameView.player.setHero(DefenderHero())
        GameView.heroPortrait.setHeroBitmap(heroBitmap)
        DefenderHero().setMultipliers()
    }

}