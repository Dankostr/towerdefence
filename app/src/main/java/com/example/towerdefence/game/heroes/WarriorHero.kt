package com.example.towerdefence.game.heroes

import com.example.towerdefence.game.gameobjects.towers.Tower

class WarriorHero: Hero {

    override val name: String = "Warrior"
    override val id: Int = 401
    override val description: String = "Warrior owns the best fighting equipment and knows how to utilize it in the battle"
    override val skills : String = "Warrior increases damage of all towers by 10%"

    //Zwieksza obrazenai zadawane przez Towery
    override fun setMultipliers() {
        Tower.damageMultiplier = 1.1f
    }
}