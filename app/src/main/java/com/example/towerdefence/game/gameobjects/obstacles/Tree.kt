package com.example.towerdefence.game.gameobjects.obstacles

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Rect
import com.example.towerdefence.data.ImageResources
import com.example.towerdefence.game.gameobjects.MapDrawable

class Tree : MapDrawable() {
        override var id: Int = 22
        override val image: Bitmap = ImageResources.getImage(id)

    private var rectHigh = Rect()

    private var background = ImageResources.getDefaultBackground()

    override fun draw(canvas: Canvas) {
        canvas.drawBitmap(background, null, getRect(), null)
        canvas.drawBitmap(image, null, rectHigh, null)
    }

    override fun setPosition(x: Int, y: Int, width: Int, height: Int) {
        super.setPosition(x, y, width, height)
        rectHigh.set(x, y - height, x + width, y + height)
    }

}