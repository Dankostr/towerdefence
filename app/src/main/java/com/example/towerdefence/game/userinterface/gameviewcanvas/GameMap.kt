package com.example.towerdefence.game.userinterface.gameviewcanvas

import android.graphics.Canvas
import android.view.MotionEvent
import com.example.towerdefence.data.ImageResources
import com.example.towerdefence.game.GameView
import com.example.towerdefence.game.gameobjects.AttackerSpawner
import com.example.towerdefence.game.gameobjects.MapDrawable
import com.example.towerdefence.game.gameobjects.attackers.*
import com.example.towerdefence.game.gameobjects.flatobjects.Grass
import com.example.towerdefence.game.gameobjects.projectiles.Projectile
import com.example.towerdefence.game.gameobjects.towers.Tower
import java.util.*
import kotlin.collections.ArrayList

/**
 * Mapa gry
 */
class GameMap(private val sizeX: Int, private val sizeY: Int) : GameViewCanvas {

    var map = Array(sizeY) { Array(sizeX) { Grass() as MapDrawable } }
    private var mapTopLeftX : Int = 0
    private var mapTopLeftY : Int = 0
    private var mapWidth : Int = 0
    private var mapHeight : Int = 0

    var mapElementWidth: Int = 0
    var mapElementHeight: Int = 0
    var spawner: AttackerSpawner =
        AttackerSpawner()
    private val towers = Collections.synchronizedList(ArrayList<Tower>())
    private val projectiles = Collections.synchronizedList(LinkedList<Projectile>())
    private val attackersOnMap = Collections.synchronizedList(LinkedList<Attacker>())

    fun getElementWidth() : Int {
        return mapElementWidth
    }

    fun getElementHeight() : Int {
        return mapElementHeight
    }

    override fun setPosition(topLeftX: Int, topLeftY: Int, width: Int, height: Int) {
        mapTopLeftX = topLeftX
        mapTopLeftY = topLeftY
        mapWidth = width
        mapHeight = height
        mapElementWidth = mapWidth / sizeX
        mapElementHeight = mapHeight / sizeY

        for (i in 0 until sizeY) {
            for (j in 0 until sizeX) {
                map[i][j].setPosition(mapTopLeftX + j * mapElementWidth, mapTopLeftY+ i * mapElementHeight,
                    mapElementWidth, mapElementHeight)
            }
        }
        // for (i in 0 until 5)
        //   attackersOnMap[i].spawnAttacker(this,i)
    }

    fun spawn(mob : Attacker) {
        attackersOnMap.add(mob)
    }

    fun physics() {

        if (attackersOnMap.size == 0)
            spawner.checkEndWave()

        for (attacker in attackersOnMap) {
            attacker.physics(this)
        }

        spawner.attackerSpawner(this)

        synchronized(attackersOnMap) {
            attackersOnMap.sortBy { -it.travelledDistance }
        }

        synchronized(towers) {
            for (t in towers)
                t.physics(attackersOnMap)
        }

        attackersOnMap.removeAll { !it.inGame }

        for (p in projectiles)
            p.physics()

        projectiles.removeAll { !it.isInGame() }
    }

    override fun draw(canvas: Canvas) {

        lateinit var attackersCopy : ArrayList<Attacker>
        synchronized (attackersOnMap) {
            attackersCopy = ArrayList(attackersOnMap)
        }

        lateinit var towersCopy : ArrayList<Tower>
        synchronized(towers) {
            towersCopy = ArrayList(towers)
        }

        lateinit var projectilesCopy : ArrayList<Projectile>
        synchronized(projectiles) {
            projectilesCopy = ArrayList(projectiles)
        }

        attackersCopy.sortBy { it.getRect().bottom }

        var drawnAttackers = 0
        for (i in 0 until sizeY) {
            val bottomY = map[i][0].getRect().bottom
            for (j in sizeX - 1 downTo  0) {
                map[i][j].draw(canvas)
            }
            while (drawnAttackers < attackersCopy.size && attackersCopy[drawnAttackers].getRect().bottom <= bottomY) {
                attackersCopy[drawnAttackers].draw(canvas)
                drawnAttackers++
            }
        }
        while (drawnAttackers < attackersCopy.size) {
            attackersCopy[drawnAttackers].draw(canvas)
            drawnAttackers++
        }

        for (t in towersCopy)
            t.drawAttack(canvas)

        for (p in projectilesCopy)
            if (p.isInGame())
                p.draw(canvas)
    }

    fun drawWithoutPhysics(canvas: Canvas) {
        var drawnAttackers = 0

        for (i in 0 until sizeY) {
            val bottomY = map[i][0].getRect().bottom
            for (j in 0 until sizeX) {
                map[i][j].draw(canvas)
            }
            while (drawnAttackers < attackersOnMap.size && attackersOnMap[drawnAttackers].getRect().bottom <= bottomY) {
                attackersOnMap[drawnAttackers].draw(canvas)
                drawnAttackers++
            }
        }

        for (i in projectiles.size - 1 downTo 0) {
            projectiles[i].physics()
            if (projectiles[i].isInGame())
                projectiles[i].draw(canvas)
            else
                removeProjectile(projectiles[i])
        }
    }

    override fun isInside(x : Int, y : Int) : Boolean {
        return (x in (mapTopLeftX .. mapTopLeftX + mapWidth) &&
                y in (mapTopLeftY .. mapTopLeftY + mapHeight))
    }

    override fun onTouch(event: MotionEvent) {
        if (event.action == MotionEvent.ACTION_UP) {
            val x = event.x.toInt() - mapTopLeftX
            val y = event.y.toInt() - mapTopLeftY
            val i = x * sizeX / mapWidth
            val j = y * sizeY / mapHeight
            val tower = GameView.selectedTower
            GameView.selectedTower = null
            if  (tower != null && isFree(i, j) && GameView.player.getGold() >= tower.cost) {
                tower.setBackground(ImageResources.getDefaultBackground())
                GameView.player.decreaseGold(tower.cost)
                setField(i, j, tower)
                towers.add(tower)
            }
        }
    }

    //sprawdza czy pole na danych wspolrzednych nadaje sie do postawienia towera (jest wolne)
    private fun isFree(i : Int, j : Int) : Boolean {
        return map[j][i].id == 0
    }

    fun setField(x: Int, y: Int, mapElement: MapDrawable) {
        map[y][x] = mapElement.apply { setPosition(mapTopLeftX  + x * mapElementWidth,
            mapTopLeftY + y * mapElementHeight, mapElementWidth, mapElementHeight) }
    }

    fun getSizeX() : Int {
        return sizeX
    }

    fun getSizeY() : Int {
        return sizeY
    }

    fun addProjectile(projectile: Projectile) {
        this.projectiles.add(projectile)
    }

    private fun removeProjectile(projectile: Projectile){
        this.projectiles.remove(projectile)
    }

    /*
    private fun sortAttackers() {
        attackersOnMap.sortBy { it.getRect().bottom }
    }*/
}