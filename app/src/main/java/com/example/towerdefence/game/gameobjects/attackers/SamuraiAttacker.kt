package com.example.towerdefence.game.gameobjects.attackers

import android.graphics.Bitmap
import com.example.towerdefence.data.ImageResources

class SamuraiAttacker : Attacker() {
    override var id: Int = 106
    override var damage: Int = 15
    override var description: String = "The most honorable warrior"
    override var health: Int = 200
    override var moveSpeed: Float = 0.01f
    override var reward: Int = 12
    override var visibleName: String = "Samurai"

    override val image: Bitmap = ImageResources.getImage(id)
}