package com.example.towerdefence.game.gameobjects.attackers

import android.graphics.Bitmap
import com.example.towerdefence.data.ImageResources

class UFOAttacker : Attacker() {
    override var id: Int = 104
    override var damage: Int = 25
    override var description: String = "Unidentified Attacking Object"
    override var health: Int = 200
    override var moveSpeed: Float = 0.02f
    override var reward: Int = 10
    override var visibleName: String = "UAO"

    override val image: Bitmap = ImageResources.getImage(id)
}