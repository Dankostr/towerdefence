package com.example.towerdefence.game.gameobjects.towers.laser_towers

import android.graphics.Canvas
import android.graphics.Paint
import com.example.towerdefence.data.ColorsResources
import com.example.towerdefence.game.gameobjects.towers.Tower
import kotlin.math.roundToInt

abstract class LaserTower : Tower() {


    open var laserColor = "red"

    override fun drawAttack(canvas: Canvas) {
        if (shooting) {
            val p = Paint()
            p.color = ColorsResources.getColor(laserColor)!!
            drawLaser(canvas, p)

        }
    }
    fun drawLaser(canvas: Canvas, paint: Paint) {
        if (shotAttacker == null)
            return
        val r = shotAttacker!!.getRect()
        canvas.drawLine(
            ((getRect().left + getRect().right) / 2).toFloat(),
            ((getRect().top + getRect().bottom) / 2).toFloat(),
            r.exactCenterX(),
            r.exactCenterY(),
            paint
        )
    }
    init{
        ColorsResources.initiate()
    }
    override fun attack() {
        if (shotAttacker!!.getDamage((damage * damageMultiplier).roundToInt())) {
            shooting = false
            shotAttacker = null
        }
        frames = 0.0
    }
}