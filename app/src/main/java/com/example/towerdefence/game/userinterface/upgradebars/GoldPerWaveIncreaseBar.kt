package com.example.towerdefence.game.userinterface.upgradebars

import com.example.towerdefence.data.ImageResources
import com.example.towerdefence.game.GameView
import com.example.towerdefence.game.gameobjects.Player

class GoldPerWaveIncreaseBar : UpgradeBar() {

    private val GOLD_INCREASE_BITMAP_ID = 904

    init {
        buttonBitmap = ImageResources.getImage(INCREASE_BITMAP_ID)
        iconBitmap = ImageResources.getImage(GOLD_INCREASE_BITMAP_ID)
        firstLine = "Increases gold received per wave by ${Player.GOLD_PER_WAVE_PER_LEVEL} gold"
        secondLine = "Current bonus: ${GameView.player.getGoldPerWaveLevel() * Player.GOLD_PER_WAVE_PER_LEVEL} gold/wave"
        costLine = "Cost: ${GameView.player.getGoldPerWaveLevel() * 12 + 10} gold"
    }

    override fun increasePlayersLevel() {
        GameView.player.upgradeGoldPerWave()
    }

    override fun calculateCost(): Int {
        secondLine = "Current bonus: ${GameView.player.getGoldPerWaveLevel() * 10} gold/wave"
        costLine = "Cost: ${GameView.player.getGoldPerWaveLevel() * 12 + 10} gold"
        return GameView.player.getGoldPerWaveLevel() * 12 + 10
    }

}