package com.example.towerdefence.game.userinterface.gameviewcanvas

import android.graphics.Canvas
import android.graphics.Paint
import android.view.MotionEvent
import com.example.towerdefence.game.GameView
import com.example.towerdefence.game.userinterface.heroes.HeroIconBar

class HeroCanvas : GameViewCanvas {


    private var canvasTopLeftX: Int = 0
    private var canvasTopLeftY: Int = 0
    private var canvasWidth: Int = 0
    private var canvasHeight: Int = 0
    private val paint: Paint = Paint()

    init {
        paint.setARGB(127, 169, 169, 169)
    }

    private val heroIconBar = HeroIconBar()
    override fun setPosition(topLeftX: Int, topLeftY: Int, width: Int, height: Int) {
        canvasTopLeftX = topLeftX
        canvasTopLeftY = topLeftY
        canvasWidth = width
        canvasHeight = height

        val barTopLeftX = canvasTopLeftX
        val barTopLeftY = (canvasTopLeftY + canvasHeight) / 2
        val barHeight = (width * 0.2).toInt()
        val barWidth = canvasWidth
        heroIconBar.setPosition(barTopLeftX, barTopLeftY, barWidth, barHeight)

    }

    override fun draw(canvas: Canvas) {
        //Log.d("draw", "Economy")
        GameView.map.drawWithoutPhysics(canvas)
        canvas.drawRect(
            canvasTopLeftX.toFloat(), canvasTopLeftY.toFloat(), canvasTopLeftX + canvasWidth.toFloat(), canvasTopLeftY +
                    canvasHeight.toFloat(), paint
        )
        heroIconBar.draw(canvas)
    }

    override fun isInside(x: Int, y: Int): Boolean {
        return (x in (canvasTopLeftX..canvasTopLeftX + canvasWidth) &&
                y in (canvasTopLeftY..canvasTopLeftY + canvasHeight))
    }

    override fun onTouch(event: MotionEvent) {
        if (event.action == MotionEvent.ACTION_DOWN) {
            val x = event.x.toInt()
            val y = event.y.toInt()
            when {
                heroIconBar.isInside(x, y) -> heroIconBar.onTouch(event)
            }
        }
    }

}