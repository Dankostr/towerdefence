package com.example.towerdefence.game.gameobjects.attackers



import android.graphics.Bitmap
import android.graphics.Canvas
import com.example.towerdefence.data.ImageResources

class NinjaAttacker : Attacker() {
    override var id: Int = 105
    override var damage: Int = 9
    override var description: String =
        "You do not see him but if you do you are already dead. (お前はもう死んでいる read: Omae Wa Mou Shindeiru)" //Nani?
    override var health: Int = 100
    override var moveSpeed: Float = 0.01f
    override var reward: Int = 8
    override var visibleName: String = "Ninja"

    override val image: Bitmap = ImageResources.getImage(id)

    override fun draw(canvas: Canvas) {
        if (health <= 50)
            super.draw(canvas)
    }
}