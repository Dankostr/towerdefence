package com.example.towerdefence.game.heroes

import com.example.towerdefence.game.gameobjects.attackers.Attacker

class MageHero: Hero {

    override val name: String = "Mage"
    override val id: Int = 404
    override val description: String = "You shall not pass!"
    override val skills : String = "Mage decreases all opponents' movement speed by 20%"

    override fun setMultipliers() {
        Attacker.speedMultiplier = 0.8f
    }
}