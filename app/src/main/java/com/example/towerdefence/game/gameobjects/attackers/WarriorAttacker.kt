package com.example.towerdefence.game.gameobjects.attackers


import android.graphics.Bitmap
import com.example.towerdefence.data.ImageResources

class WarriorAttacker : Attacker() {
        override var id: Int = 101
        override var damage: Int = 10
        override var description: String = "Mad and dangerous"
        override var health: Int = 150
        override var moveSpeed: Float = 0.015f
        override var reward: Int = 4
        override var visibleName: String = "Warrior"

        override val image: Bitmap = ImageResources.getImage(id)
}