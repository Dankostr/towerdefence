package com.example.towerdefence.game.userinterface.upgradebars

import com.example.towerdefence.data.ImageResources
import com.example.towerdefence.game.GameView
import com.example.towerdefence.game.gameobjects.Player

class TowerDamageIncreaseBar : UpgradeBar() {

    private val TOWER_DAMAGE_BITMAP_ID = 202

    init {
        buttonBitmap = ImageResources.getImage(INCREASE_BITMAP_ID)
        iconBitmap = ImageResources.getImage(TOWER_DAMAGE_BITMAP_ID)
        firstLine = "Increases tower damage by ${Player.TOWER_DAMAGE_PER_LEVEL} %"
        secondLine = "Current bonus: ${GameView.player.getTowerDamageLevel() * Player.TOWER_DAMAGE_PER_LEVEL} %"
        costLine = "Cost: ${GameView.player.getTowerDamageLevel() * 15 + 10} gold"
    }

    override fun increasePlayersLevel() {
        GameView.player.upgradeTowerDamage()
    }

    override fun calculateCost(): Int {
        secondLine = "Current bonus: ${GameView.player.getTowerDamageLevel() * Player.TOWER_DAMAGE_PER_LEVEL} %"
        costLine = "Cost: ${GameView.player.getTowerDamageLevel() * 15 + 10} gold"
        return GameView.player.getTowerDamageLevel() * 15 + 10
    }
}