package com.example.towerdefence.game.userinterface.upgradebars

import android.graphics.*
import android.util.Log
import android.view.MotionEvent
import com.example.towerdefence.game.GameView
import com.example.towerdefence.game.userinterface.gameviewcanvas.GameViewCanvas

abstract class UpgradeBar : GameViewCanvas {

    protected val INCREASE_BITMAP_ID = 911

    private var barTopLeftX : Int = 0
    private var barTopLeftY : Int = 0
    private var barWidth : Int = 0
    private var barHeight : Int = 0

    private val iconRect = Rect()
    private val firstLineRect = Rect()
    private val secondLineRect = Rect()
    private val costLineRect = Rect()
    private val buttonRect = Rect()

    protected var firstLine = ""
    protected var secondLine = ""
    protected var costLine = ""

    protected lateinit var iconBitmap: Bitmap
    protected lateinit var buttonBitmap: Bitmap

    protected var cost: Int = calculateCost()

    override fun setPosition(topLeftX: Int, topLeftY: Int, width: Int, height: Int) {
        barTopLeftX = topLeftX
        barTopLeftY = topLeftY
        barWidth = width
        barHeight = height

        val spacingX = 0
        val spacingY = (barHeight * 0.3).toInt()
        val iconSize = Math.min(barHeight - spacingY, (barWidth * 0.25).toInt())
        val buttonSize = (0.5 * iconSize).toInt()
        val textWidth = (barWidth - iconSize - buttonSize - 4 * spacingX)
        val iconYDiff = (barHeight - iconSize) / 2
        val buttonYDiff = (barHeight - buttonSize) / 2

        iconRect.set(barTopLeftX + spacingX, barTopLeftY + iconYDiff,
            barTopLeftX + spacingX + iconSize, barTopLeftY + iconYDiff + iconSize)
        firstLineRect.set(barTopLeftX , barTopLeftY,
            barTopLeftX + textWidth, barTopLeftX + iconYDiff + iconSize/2)
        secondLineRect.set(barTopLeftX, barTopLeftY + (barHeight * 0.15).toInt(),
            barTopLeftX + textWidth, barTopLeftX + iconYDiff + iconSize)
        costLineRect.set(barTopLeftX + spacingX + iconSize + 10, barTopLeftY + buttonYDiff,
            barTopLeftX + 3 * spacingX + textWidth + iconSize, barTopLeftY + buttonYDiff + buttonSize)
        buttonRect.set(barTopLeftX + 3 * spacingX + textWidth + iconSize, barTopLeftY + buttonYDiff,
            barTopLeftX + 3 * spacingX + textWidth + iconSize + buttonSize, barTopLeftY + buttonYDiff + buttonSize)
    }

    override fun draw(canvas: Canvas) {
        //Log.d("draw", "UpgradeBar")
        val paint = Paint()
        paint.textSize = 24f
        canvas.drawBitmap(iconBitmap, null, iconRect, null)
        canvas.drawText(firstLine, firstLineRect.left.toFloat(), firstLineRect.top.toFloat(), paint)
        canvas.drawText(secondLine, secondLineRect.left.toFloat(), secondLineRect.top.toFloat(), paint)
        canvas.drawText(costLine, costLineRect.left.toFloat(), costLineRect.top.toFloat(), paint)
        canvas.drawBitmap(buttonBitmap, null, buttonRect, null)
    }

    override fun isInside(x: Int, y: Int): Boolean {
        return (x in (barTopLeftX .. barTopLeftX + barWidth) &&
                y in (barTopLeftY .. barTopLeftY + barHeight))
    }

    override fun onTouch(event: MotionEvent) {
        if (event.action == MotionEvent.ACTION_DOWN) {
            val x = event.x.toInt()
            val y = event.y.toInt()
            if (buttonRect.contains(x, y)) {
                upgradeButtonClicked()
            }
        }
    }

    private fun upgradeButtonClicked() {
        if (GameView.player.getGold() >= this.cost) {
            GameView.player.decreaseGold(this.cost)
            increasePlayersLevel()
            this.cost = calculateCost()
        }
    }
    abstract fun increasePlayersLevel()

    abstract fun calculateCost() : Int
}