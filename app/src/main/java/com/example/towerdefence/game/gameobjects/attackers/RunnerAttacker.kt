package com.example.towerdefence.game.gameobjects.attackers

import android.graphics.Bitmap
import com.example.towerdefence.data.ImageResources

class RunnerAttacker : Attacker() {
    override var id: Int = 110
    override var damage: Int = 15
    override var description: String = "Gotta go fast"
    override var health: Int = 100
    override var moveSpeed: Float = 0.005f
    override var reward: Int = 10
    override var visibleName: String = "Runner"

    override val image: Bitmap = ImageResources.getImage(id)

    override fun getDamage(dmg: Int): Boolean {
        health -= dmg
        moveSpeed += 0.005f
        if (health <= 0) {
            die()
            return true
        }
        return false
    }
}