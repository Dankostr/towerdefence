package com.example.towerdefence.game.gameobjects.attackers


import android.graphics.Bitmap
import com.example.towerdefence.data.ImageResources

class MobAttacker : Attacker() {
        override var id: Int = 100
        override var damage: Int = 1
        override var description: String = "The most basic attacker"
        override var health: Int = 75
        override var moveSpeed: Float = 0.01f
        override var reward: Int = 2
        override var visibleName: String = "Mob"

        override val image: Bitmap = ImageResources.getImage(id)
}