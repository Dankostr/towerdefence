package com.example.towerdefence.game.map

import com.example.towerdefence.data.ObjectCreator
import com.example.towerdefence.data.Save
import com.example.towerdefence.game.gameobjects.attackers.Attacker
import com.example.towerdefence.game.gameobjects.attackers.BlackDragonAttacker
import com.example.towerdefence.game.userinterface.gameviewcanvas.GameMap
import kotlin.random.Random

object MapGenerator {

    fun getMissionMap(num : Int) : GameMap {
        return Save.loadMap(num)
    }

    fun getRandomMap(sizeX : Int, sizeY : Int, homeX : Int, homeY : Int) : GameMap {
        val map = RandomMap(sizeX, sizeY, homeX, homeY).createMap()
        map.spawner.setWaves(generateRandomWaves())
        return map
    }

    fun loadMobs(map: GameMap, i: Int) {
        map.spawner.setWaves(Save.loadWave(i))
    }

    private fun generateRandomWaves() : List<List<Attacker>> {
        val waves = ArrayList<ArrayList<Attacker>>()
        var begin = 0
        var end = 0
        for (i in 0..32) {
            waves.add(generateWave(begin, end, i * 2 + 5))
            if (i % 2 == 0) {
                end++
                if (end - begin > 5)
                    begin++
            }
        }
        val bs = ArrayList<Attacker>().apply { this.add(BlackDragonAttacker()) }
        waves.add(bs)
        return waves
    }

    private fun generateWave(begin : Int, end : Int, count : Int) : ArrayList<Attacker> {
        val wave = ArrayList<Attacker>()
        for (i in 0 until count)
            wave.add(getRandomAttacker(begin, end))
        return wave
    }

    private fun getRandomAttacker(begin : Int, end : Int) : Attacker {
        val rand = Random.nextInt(begin, end + 1)
        return ObjectCreator.getAttacker(rand + 'A'.toInt())
    }

}
