package com.example.towerdefence.game.gameobjects.flatobjects

import android.graphics.Bitmap
import com.example.towerdefence.data.ImageResources
import com.example.towerdefence.game.gameobjects.MapDrawable

class Gravel : MapDrawable() {
        override var id: Int = 3
        override val image: Bitmap = ImageResources.getImage(id)
}