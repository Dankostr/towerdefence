package com.example.towerdefence.game.gameobjects.flatobjects

import android.graphics.Bitmap
import com.example.towerdefence.data.ImageResources
import com.example.towerdefence.game.gameobjects.MapDrawable

class Brick : MapDrawable() {
    override var id: Int = 1
    override val image: Bitmap = ImageResources.getImage(id)
}