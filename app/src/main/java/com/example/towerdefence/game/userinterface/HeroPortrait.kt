package com.example.towerdefence.game.userinterface

import android.graphics.*
import android.view.MotionEvent
import com.example.towerdefence.game.GameView

class HeroPortrait {

    private var barTopLeftX : Int = 0
    private var barTopLeftY : Int = 0
    private var barWidth : Int = 0
    private var barHeight : Int = 0

    private val imageRect = Rect()
    private var image: Bitmap? = null

    private val borderWidth : Int = 8

    fun setPosition(topLeftX: Int, topLeftY: Int, width: Int, height: Int) {
        barTopLeftX = topLeftX
        barTopLeftY = topLeftY
        barWidth = width
        barHeight = height

        imageRect.set(barTopLeftX + borderWidth,
            barTopLeftY + borderWidth,
            barTopLeftX - borderWidth + width,
            barTopLeftY - borderWidth + height)
    }

    fun setHeroBitmap(bitmap: Bitmap?) {
        this.image = bitmap
    }

    fun draw(canvas: Canvas) {
        drawBorder(canvas)
        drawBackground(canvas)
        drawPortrait(canvas)
    }

    fun isInside(x : Int, y : Int) : Boolean {
        return (x in (barTopLeftX .. barTopLeftX + barWidth) &&
                y in (barTopLeftY .. barTopLeftY + barHeight))
    }

    fun onTouch(event: MotionEvent) {
    }

    private fun drawBorder(canvas: Canvas) {
        val paint = Paint()
        paint.color = Color.rgb(102, 51, 0)
        canvas.drawRect(Rect().apply {
            this.set(barTopLeftX, barTopLeftY, barTopLeftX+barWidth, barTopLeftY+barHeight) }, paint)
    }

    private fun drawBackground(canvas: Canvas) {
        val paint = Paint()
        paint.color = Color.rgb(255, 255, 255)
        canvas.drawRect(imageRect, paint)
    }

    private fun drawPortrait(canvas: Canvas) {
        if (image != null)
            canvas.drawBitmap(image!!, null, imageRect, null)
    }

}