package com.example.towerdefence.game.heroes

import com.example.towerdefence.game.gameobjects.attackers.Attacker

class EconomicHero: Hero {

    override val name: String = "Leprechaun"
    override val id: Int = 403
    override val description: String = "Leprechaun brings luck (and money) to his army"
    override val skills : String = "Leprechaun increases gold received from killing enemies by 10%"

    override fun setMultipliers() {
        Attacker.goldMultiplier = 1.1f
    }
}