package com.example.towerdefence.information.attackers

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import com.example.towerdefence.R
import com.example.towerdefence.data.ImageResources
import com.example.towerdefence.information.attackers.AttackerInformationStorage
import com.example.towerdefence.information.attackers.AttackersViewHolder
import java.util.*


class AttackersArrayAdapter(context: Context, private var data: ArrayList<AttackerInformationStorage>) :
    ArrayAdapter<AttackerInformationStorage>(context, R.layout.single_tower_list_item, data) {


    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val holder : AttackersViewHolder

        var view = convertView
        if (view == null) {
            val inflater = LayoutInflater.from(context)
            view = inflater.inflate(R.layout.single_attacker_list_item, parent, false)
            holder = AttackersViewHolder()
            holder.nameTextView = view.findViewById(R.id.nameTextView) as TextView
            holder.damageTextView = view.findViewById(R.id.damageTextView) as TextView
            holder.healthTextView = view.findViewById(R.id.healthTextView) as TextView
            holder.moveSpeedTextView = view.findViewById(R.id.moveSpeedTextView) as TextView
            holder.rewardTextView = view.findViewById(R.id.rewardTextView) as TextView
            holder.imageView = view.findViewById(R.id.imageView) as ImageView
            view.tag = holder
        }
        else{
            holder = view.tag as (AttackersViewHolder)
        }

        holder.nameTextView!!.text = data[position].name
        holder.damageTextView!!.text = data[position].damage.toString()
        holder.healthTextView!!.text = data[position].health.toString()
        holder.moveSpeedTextView!!.text = data[position].moveSpeed.toString()
        holder.rewardTextView!!.text = data[position].reward.toString()

        holder.imageView!!.setImageBitmap(ImageResources.getImage(data[position].id))


        return view!!
    }


}