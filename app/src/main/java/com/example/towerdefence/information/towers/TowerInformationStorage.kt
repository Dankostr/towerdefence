package com.example.towerdefence.information.towers

import java.io.Serializable

class TowerInformationStorage (val id: Int, val name : String, val damage : Int, val range : Float, val attackSpeed : Double, val description : String, val cost: Int) : Serializable