package com.example.towerdefence.information.heroes

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.towerdefence.R
import com.example.towerdefence.data.DatabaseHelper
import kotlinx.android.synthetic.main.heroes_list_fragment.*

class HeroesListFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.heroes_list_fragment, container, false)
    }
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val dbHelper = DatabaseHelper(activity as Context)
        val data = dbHelper.getAllHeroes()
        val adapter = HeroesArrayAdapter(activity as Context, data)
        listView.adapter = adapter
        listView.setOnItemLongClickListener { _, _, position, id ->
            adapter.notifyDataSetChanged()
            val myIntent = Intent(activity as Context, HeroDetailsActivity::class.java)
            myIntent.putExtra("heroInformation", data[position])
            startActivityForResult(myIntent, 123)
            true
        }
    }
}
