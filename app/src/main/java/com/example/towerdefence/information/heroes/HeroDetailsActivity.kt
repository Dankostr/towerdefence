package com.example.towerdefence.information.heroes

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.towerdefence.R
import com.example.towerdefence.data.ImageResources
import kotlinx.android.synthetic.main.hero_details.*
import kotlinx.android.synthetic.main.hero_details.descriptionTextView
import kotlinx.android.synthetic.main.hero_details.imageView
import kotlinx.android.synthetic.main.hero_details.nameTextView

class HeroDetailsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.hero_details)
        val heroInfo =  intent.getSerializableExtra("heroInformation") as HeroInformationStorage
        nameTextView.text = heroInfo.name
        skillsTextView.text = heroInfo.skills
        descriptionTextView.text = heroInfo.description
        imageView.setImageBitmap(ImageResources.getImage(heroInfo.id))

    }

}
