package com.example.towerdefence.information.towers

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.towerdefence.R
import com.example.towerdefence.data.DatabaseHelper
import kotlinx.android.synthetic.main.towers_list_fragment.*

class TowersListFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.towers_list_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val dbHelper = DatabaseHelper(activity as Context)
        val data = dbHelper.getAllTowers()
        val adapter = TowersArrayAdapter(activity as Context, data)
        listView.adapter = adapter
        listView.setOnItemLongClickListener { _, _, position, id ->
            adapter.notifyDataSetChanged()
            val myIntent = Intent(activity as Context, TowerDetailsActivity::class.java)
            myIntent.putExtra("towerInformation", data[position])
            startActivityForResult(myIntent, 123)
            true
        }
    }
}
