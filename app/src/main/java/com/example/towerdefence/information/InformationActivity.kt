package com.example.towerdefence.information

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.towerdefence.information.attackers.AttackersListFragment
import com.example.towerdefence.information.heroes.HeroesListFragment
import com.example.towerdefence.R
import com.example.towerdefence.information.towers.TowersListFragment
import kotlinx.android.synthetic.main.activity_information.*

class InformationActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_information)

        heroes_button.setOnClickListener { heroesClicked() }
        towers_button.setOnClickListener { towersClicked() }
        attackers_button.setOnClickListener { attackersClicked() }


        val sharedPref = this.getPreferences(Context.MODE_PRIVATE)
        val str = sharedPref.getString("usedFragment", "hero")
        when(str){
            "hero" -> heroesClicked()
            "tower" -> towersClicked()
            "attack" -> attackersClicked()
        }

    }

    private fun heroesClicked(){
        val newFragment = HeroesListFragment()
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.dynamic_list, newFragment)
        val sharedPref = this.getPreferences(Context.MODE_PRIVATE)
        val edit = sharedPref.edit()
        edit.putString("usedFragment", "hero")
        edit.apply()

        transaction.commit()
    }
    private fun towersClicked(){
        val newFragment = TowersListFragment()
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.dynamic_list, newFragment)
        val sharedPref = this.getPreferences(Context.MODE_PRIVATE)
        val edit = sharedPref.edit()
        edit.putString("usedFragment", "tower")
        edit.apply()

        transaction.commit()

    }
    private fun attackersClicked(){
        val newFragment = AttackersListFragment()
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.dynamic_list, newFragment)
        val sharedPref = this.getPreferences(Context.MODE_PRIVATE)
        val edit = sharedPref.edit()
        edit.putString("usedFragment", "attack")
        edit.apply()

        transaction.commit()

    }

    override fun onBackPressed() {
        val sharedPref = this.getPreferences(Context.MODE_PRIVATE)
        val edit = sharedPref.edit()
        edit.putString("usedFragment", "hero")
        edit.apply()
        super.onBackPressed()
    }
}
