package com.example.towerdefence.information.attackers

import java.io.Serializable

class AttackerInformationStorage (val id: Int, val name : String, val damage : Int, val health : Int, val moveSpeed : Float, val description : String, val reward: Int) : Serializable