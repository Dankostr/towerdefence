package com.example.towerdefence.information.heroes

import android.widget.ImageView
import android.widget.TextView

class HeroesViewHolder {
    var nameTextView : TextView?  = null
    var skillsTextView : TextView?  = null
    var imageView : ImageView? = null
}