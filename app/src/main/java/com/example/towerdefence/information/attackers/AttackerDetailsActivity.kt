package com.example.towerdefence.information.attackers

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.towerdefence.R
import com.example.towerdefence.data.ImageResources
import kotlinx.android.synthetic.main.attacker_details.*

class AttackerDetailsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.attacker_details)
        val attackerInfo =  intent.getSerializableExtra("attackerInformation") as AttackerInformationStorage
        nameTextView.text = attackerInfo.name
        damageTextView.text = attackerInfo.damage.toString()
        healthTextView.text = attackerInfo.health.toString()
        descriptionTextView.text = attackerInfo.description
        moveSpeedTextView.text = attackerInfo.moveSpeed.toString()
        rewardTextView.text = attackerInfo.reward.toString()
        imageView.setImageBitmap(ImageResources.getImage(attackerInfo.id))

    }

}
