package com.example.towerdefence.information.attackers

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.towerdefence.R
import com.example.towerdefence.data.DatabaseHelper
import com.example.towerdefence.information.attackers.AttackersArrayAdapter
import kotlinx.android.synthetic.main.attackers_list_fragment.*

class AttackersListFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.attackers_list_fragment, container, false)
    }
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val dbHelper = DatabaseHelper(activity as Context)
        val data = dbHelper.getAllAttackers()
        val adapter = AttackersArrayAdapter(activity as Context, data)
        listView.adapter = adapter
        listView.setOnItemLongClickListener { _, _, position, id ->
            adapter.notifyDataSetChanged()
            val myIntent = Intent(activity as Context, AttackerDetailsActivity::class.java)
            myIntent.putExtra("attackerInformation", data[position])
            startActivityForResult(myIntent, 123)
            true
        }
    }
}

