package com.example.towerdefence

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.Button
import com.example.towerdefence.data.ImageResources
import com.example.towerdefence.game.GameActivity
import kotlinx.android.synthetic.main.capaign_activity.*

class CampaignActivity : AppCompatActivity() {

    private val CAMPAIGN_CODE = 15

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.capaign_activity)
        val preferences = PreferenceManager.getDefaultSharedPreferences(applicationContext)
        var levels = 1
        if (preferences.contains("Level")) {
            levels = preferences.getInt("Level", 1)
        } else {
            preferences.edit().putInt("Level", 1).apply()
        }
        button.isEnabled = true
        for (i in 2..8) {
            if (i > levels)
                findViewById<Button>(
                    resources.getIdentifier(
                        "button" + i.toString(),
                        "id",
                        this.packageName
                    )
                ).isEnabled = false
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CAMPAIGN_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                val intent = Intent()
                intent.putExtra("Won", data!!.getIntExtra("Won", 1))
                setResult(Activity.RESULT_OK, intent)
                finish()
            } else {
                val intent = Intent()
                setResult(Activity.RESULT_CANCELED, intent)
                finish()
            }

        }
    }

    fun levelClicked(view: View) {
        val intent = Intent(this, GameActivity::class.java)
        intent.putExtra("Campaign", (view as Button).text.toString().toInt())
        startActivityForResult(intent, CAMPAIGN_CODE)
    }

    fun resetProgress(view: View) {
        val preferences = PreferenceManager.getDefaultSharedPreferences(applicationContext)
        preferences.edit().putInt("Level", 1).apply()

        button.isEnabled = true
        for (i in 2..8) {
            findViewById<Button>(
                resources.getIdentifier(
                    "button" + i.toString(),
                    "id",
                    this.packageName
                )
            ).isEnabled = false
        }
    }

}